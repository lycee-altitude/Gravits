    remise_a_zero_courbe() :
        Cette fonction réinitialise les listes globales courbe_x_vitesse, courbe_y_temp, courbe_x_position, et courbe_x_force.

    variable_dessin(valeur_x_vitesse, valeur_y_temp, valeur_x_position, hauteur_rebond, valeur_x_force) :
        Cette fonction ajoute les valeurs passées en paramètres aux listes globales courbe_x_vitesse, courbe_y_temp, courbe_x_position, et courbe_x_force.

    dessin() :
        Cette fonction crée trois graphiques :
            Le premier graphique affiche la vitesse en fonction du temps.
            Le deuxième graphique affiche la position en fonction du temps.
            Le troisième graphique affiche la force de frottement en fonction du temps.
        Chaque graphique est sauvegardé en tant qu'image PNG dans le dossier "courbe" et affiché à l'écran.

    pdf(constante, evolution) :
        Cette fonction génère un fichier PDF contenant des informations sur l'étude du mouvement.
        Elle inclut des données sur les constantes et les caractéristiques du mouvement telles que la vitesse maximale, la position maximale, etc.
        Les graphiques générés par la fonction dessin() sont insérés dans le PDF.
        Le fichier PDF est sauvegardé sous le nom "fiche_etude_du_mouvement.pdf".