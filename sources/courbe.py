#https://python.doctor/page-creer-graphiques-scientifiques-python-apprendre
import matplotlib.pyplot as plt
from fpdf import FPDF
    
def remise_a_zero_courbe():
    global courbe_x_vitesse
    global courbe_y_temp
    global courbe_x_position
    global courbe_x_force
    courbe_x_force=[]
    courbe_x_vitesse=[]
    courbe_y_temp=[]
    courbe_x_position=[]
def variable_dessin(valeur_x_vitesse,valeur_y_temp,valeur_x_position,hauteur_rebond,valeur_x_force):
    global courbe_x_vitesse
    courbe_x_vitesse.append(round(valeur_x_vitesse,4))
    global courbe_y_temp
    courbe_y_temp.append(round(valeur_y_temp,4))
    global courbe_x_position
    courbe_x_position.append(round(-valeur_x_position+hauteur_rebond,1))
    global courbe_x_force
    courbe_x_force.append(round(valeur_x_force,4))


def dessin():
    plt.grid(True)
    plt.plot(courbe_y_temp, courbe_x_vitesse, "c", linewidth=0.8, label="Vitesse/temps")
    plt.text( len(courbe_y_temp)+3,max(courbe_x_vitesse), r'Vitesse_max ='+str(max(courbe_x_vitesse)))
    plt.text( len(courbe_y_temp)+3,min(courbe_x_vitesse), r'Vitesse_min ='+str(min(courbe_x_vitesse)))
    plt.axis([0, len(courbe_y_temp), min(courbe_x_vitesse)-10, max(courbe_x_vitesse)+10])
    plt.xlabel('Temps (en s)')
    plt.ylabel('Vitesse (en m/s)')
    plt.legend() 
    plt.savefig("courbe/vitesse",dpi=300)    
    plt.show()

    plt.grid(True)
    plt.plot(courbe_y_temp, courbe_x_position, "r", linewidth=0.8, label="Position/temps")
    plt.axis([0, len(courbe_y_temp)+3, min(courbe_x_position)-10, max(courbe_x_position)+10])
    plt.xlabel('Temps (en s)')
    plt.ylabel('Position (en m)')
    plt.legend()    
    plt.savefig("courbe/position",dpi=300)  
    plt.show()
    
    
    plt.plot(courbe_y_temp, courbe_x_force, "g", linewidth=0.8, label="ForceFrottement/temps")
    plt.text(len(courbe_y_temp)+3,max(courbe_x_force), r'ForceFrottement_max ='+str(max(courbe_x_force)))
    plt.axis([0, len(courbe_y_temp), min(courbe_x_force)-10, max(courbe_x_force)+10])
    plt.xlabel('Temps (en s)')
    plt.ylabel('FF (en N)')
    plt.legend()    
    plt.savefig("courbe/frottement",dpi=300)  
    plt.show()
    
def pdf(constante,evolution):
    pdf = FPDF()
    pdf.add_page()
    pdf.set_font("Times", "IB",12)              #IB -> italique et gras
    pdf.cell(0, 14, txt="Etude de la chute", ln=1, align="C") 
    pdf.set_font("Helvetica", "I",8)
    constante["position 0"]=str(courbe_x_position[0])+"m"
    constante["vitesse 0"]=str(courbe_x_vitesse[0])+"m/s"
    for i in constante:
        texte=str(i)+" = "+str(constante[i]) + "\n"
        pdf.cell(0, 10, texte, border=1, align="C") #cell -> cellule, ici le 0 permet de la définir allant jusqu'à la marge à droite, le 10 définit la hauteur, border -> bordure apparente, align("C") -> met au centre de la celule le txt
        pdf.ln(10)
    pdf.ln(10)
    
    pdf.set_font("Times", "B",10)
    pdf.cell(0, 10, txt="LA VITESSE",align="C")
    pdf.ln(10)
    pdf.set_font("Arial", "",8)
    pdf.image("courbe/vitesse.png",w=100,h=100)
    pdf.multi_cell(0, 5, txt=f"Nous retrouvons une balle qui atteint la vitesse maximum de {str(max(courbe_x_vitesse))}m/s au temps t={courbe_x_vitesse.index(max(courbe_x_vitesse))}s et une vitesse minimum de {str(min(courbe_x_vitesse))}m/s au temps t={courbe_x_vitesse.index(min(courbe_x_vitesse))}s.", align="L")#on utilise multi-cell car cela permet d'avoir des retour auto à la ligne 
    pdf.ln(5)    
    if max(courbe_x_force)!=0: pdf.cell(0, 10, txt=f"Si nous voulions calculer la vitesse limite alors nous ferions le calcul suivant: Vlim = g * T = {constante['constante de gravitation']} * {constante['T (constante : m/k)']} = {constante['vitesse limite']}m/s " , ln=1, align="L")  
    
    
    pdf.set_font("Times", "B",10)
    pdf.cell(0, 10, txt="LA POSITION",align="C")
    pdf.ln(1)    
    pdf.set_font("Arial", "",8)
    pdf.image("courbe/position.png",w=100,h=100)

    liste_maximum_locaux=""
    max_local=0
    distance=-courbe_x_position[0]
    nb_maximum_locaux=0
    for i in range (1,len(courbe_x_position)):
        if courbe_x_position[i-1]>courbe_x_position[i]:
            if max_local==0:
                print("v")
                distance=distance + courbe_x_position[i]*2
                liste_maximum_locaux=liste_maximum_locaux+str(courbe_x_position[i-1])+"m"+" au temps t="+str(i)+", "
                nb_maximum_locaux=nb_maximum_locaux+1
                max_local=1
            if i==len(courbe_x_position)-1: print(distance,courbe_x_position[i]); distance=distance-courbe_x_position[i];
        elif courbe_x_position[i-1]<courbe_x_position[i]:
            max_local=0
            if i==len(courbe_x_position)-1:
                nb_maximum_locaux=nb_maximum_locaux+1
                distance=distance+courbe_x_position[i]
                liste_maximum_locaux=liste_maximum_locaux+str(courbe_x_position[i-1])+"m"+" au temps t="+str(i)+". "

    pdf.cell(0, 10, txt="La hauteur maximale est de "+str(max(courbe_x_position))+"m. Elle parcourt une distance total de "+str(round(distance,2))+"m.", ln=1, align="L")  
    pdf.cell(0, 10, txt=f"Ainsi on observe {nb_maximum_locaux} maximum locaux distincts: "+liste_maximum_locaux, ln=1, align="L") 
    if max(courbe_x_force)!=0:
        pdf.set_font("Times", "B",10)
        pdf.cell(0, 10, txt="LES FROTTEMENTS",align="C")
        pdf.ln(10)
        pdf.set_font("Arial", "",8)
        pdf.image("courbe/frottement.png",w=100,h=100)
        pdf.cell(0, 10, txt="La force de frottement maximale est de "+str(max(courbe_x_position))+"N.", ln=1, align="L")    

    pdf.output("fiche_etude_du_mouvement.pdf")
