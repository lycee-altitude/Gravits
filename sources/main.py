import pygame
import pandas
from valeurdeketT import * 
from courbe import *
from tkinter import *
from tkinter import messagebox
Tk().wm_withdraw()                          #cache la fenêtre basique et ouverte automatiquement par tkinter


#initialisation de pygame
pygame.init()
screen = pygame.display.set_mode((0,0),pygame.FULLSCREEN)
global SCREENWIDTH, SCREENHEIGHT
SCREENWIDTH, SCREENHEIGHT = screen.get_size()
clock = pygame.time.Clock()
main_background = pygame.transform.scale(pygame.image.load('image/main_background.jpg'),(SCREENWIDTH, SCREENHEIGHT))
ciel = pygame.transform.scale(pygame.image.load('image/ciel.png'),(SCREENWIDTH, SCREENHEIGHT*0.615))
programIcon = pygame.image.load('image/icone.png')
pygame.display.set_icon(programIcon)
pygame.display.set_caption("Gravit's")
running = True
t = 0 

#csv donnees coefficient de restitution
global liste_e 
tableau_e = pandas.read_csv("valeur_e.csv") 
liste_materiaux_zone = tableau_e['solide_1'][:]
liste_materiaux = tableau_e['solide_2'][:]
liste_e = tableau_e['e'][:]

#initialisation texte pygame
font = pygame.font.Font("font/roboto.ttf", round(0.05 * SCREENHEIGHT))
font_Mid = pygame.font.Font("font/roboto.ttf", round(0.065 * SCREENHEIGHT))
font_Large = pygame.font.Font("font/roboto.ttf", round(0.09 * SCREENHEIGHT))
color = (0,0,0)
couleur_materiau = (0,0,0)

#initialisation son pygame
son=pygame.mixer.Sound("boing.mp3")
son_pdf=pygame.mixer.Sound("pdf.mp3")



# Objet qui rebondit
class objet:
    def __init__(self, masse, index_materiau, coeff_aer, pos_init, rayon, vitesse_init):
        self.rayon=rayon
        self.masse = masse
        self.vitesse_init = vitesse_init
        self.vitesse = self.vitesse_init
        self.e=float(liste_e[index_materiau])                              #coeff de restitution
        self.Cx = coeff_aer                                         #coeff frottement air
        self.pos = pos_init
        self.pos_init = pos_init
        self.force=0
        self.evolution={}                                               #permettra de faciliter l'étude
        
    def position_update(self, t):                                   #methode de deplacement de la balle, utilisant les formules de physique
        print("---------------------------------")
        print("self.vitesse AV   = ", self.vitesse)
        valeur_de_base(g,viscosite,rayon,self.masse)
        self.vitesse = vitesse_avec_frottement(t,self.pos_init,self.vitesse_init)
        self.pos = position_avec_frottement(t,self.pos_init,self.vitesse_init)
        self.force=self.vitesse*k_vitesse_lent( )
        self.evolution[t]=(self.vitesse,self.pos,self.force)
        print(self.force)
        #self.vitesse = (g * (t)) + self.vitesse_init
        #self.pos = ((g * (t**2))/2) + (self.vitesse_init)*t + self.pos_init
        return self.pos
    
    def rebond(self):                                               #methode pour le rebond
        """ Inverse le sens de la vitesse + l'affaiblit"""
        self.pos_init = hauteur_rebond
        self.vitesse_init = -  (self.vitesse * self.e)
        
    def rayon_maxetmin(self):
        if self.rayon >= 60:
            return 60
        elif self.rayon <= 10:
            return 10
        else:
            return self.rayon
        
        
# Variable modifiables par l'utilisateur
class variables:
    def __init__(self,posy,texte,nom,modif):
        self.ismodifiable = modif
        if self.ismodifiable:
            self.posx = 0.055 * SCREENWIDTH                                             #toutes ces variables sont sur la meme position en x
        else:
            self.posx = 0.78 *SCREENWIDTH
        self.posy = posy * SCREENHEIGHT
        self.text = texte                                           #contenu de la variable
        self.nom = nom
        self.active = False                                         #si elle est en train de se faire modifier
        self.color = (0,0,0)                                        #couleur de la variable, purement graphique (aide à savoir quelle variable est active)
        self.box = pygame.Rect(self.posx,self.posy,0.19 * SCREENWIDTH, 0.065 * SCREENHEIGHT)          #boite de collision de pygame

    def change_variable(self):

        if event.type == pygame.MOUSEBUTTONDOWN:                    #si clique gauche de la souris
            if self.box.collidepoint(event.pos):                    #si c'est sur la boite de co llision
                self.active = True
                self.text = ""
            else:
                self.active = False
            if self.active == True:
                self.color = (80,0,230)
            else:
                self.color = (0,0,0)
        if event.type == pygame.KEYDOWN:                            #si clavier utilisé
            if self.active:                                         #si variable active
                if event.key == pygame.K_RETURN:                    #si touche entrer
                    self.active = False
                    self.color = (0,0,0)
                    return self.text                                #recuperer le contenu de la variable
                elif event.key == pygame.K_BACKSPACE:               #si touche retour
                    self.text = self.text[:-1]                      #enlever une lettre
                else:
                    self.text += event.unicode                      #sinon ajouter touche du clavier
                            

    def affiche_variable(self):
        
        if self.nom == 'viscosité' or self.nom == 'vit. init':
            screen.fill((180,100,250),(self.box.x,self.box.y+5,self.box[2],self.box[3]*1.7))
            pygame.draw.rect(screen,(0,0,0),(self.box.x,self.box.y+5,self.box[2],self.box[3]*1.7),5)
            screen.blit(font.render(self.nom + ":",True,self.color),(self.box.x+10, self.box.y-5))
            screen.blit(font.render(self.text,True,self.color),(self.box.x+0.05 * SCREENHEIGHT, self.box.y + 0.03 * SCREENWIDTH))
        else:
            if self.ismodifiable:
                screen.fill((180,100,250),self.box)
                pygame.draw.rect(screen,(0,0,0),self.box,5)
                screen.blit(font.render(self.nom + ":" + self.text, True, self.color),(self.box.x+10, self.box.y-5))  #afficher la variable : blit(texte à ecrire(format special a pygame), coordonnees)
            else:
                screen.fill((250,150,150),(self.box.x,self.box.y,self.box[2],self.box[3]*1.2))
                pygame.draw.rect(screen,(0,0,0),(self.box.x,self.box.y,self.box[2],self.box[3]*1.2),5)
                screen.blit(font_Mid.render(self.nom + ":" + self.text, True, self.color),(self.box.x+10, self.box.y-5))


#declaration
#valeurs initiales
liste_var_obj = [round(0.095 * SCREENHEIGHT),100,None,9.81,10,0]
index_materiaux = 0     #materiau actuel
hauteur_rebond = 0.7 * SCREENHEIGHT   #coordonnee y à laquelle la balle va rebondir
new_index_materiaux = 0
nb_rebond = 0
temps_global=0
remise_a_zero_courbe()
inchange=1                          #permet de définir quand il y a des changement sur la courbe
pause_box = pygame.Rect(0.75 * SCREENWIDTH, 0.65 * SCREENHEIGHT, 0.18 * SCREENWIDTH,0.22 * SCREENHEIGHT)
pdf_toggle = False
#instanciation objet rebondissant
boule = objet(liste_var_obj[1],index_materiaux,None,liste_var_obj[0],liste_var_obj[4],liste_var_obj[5])
#instanciation variables
#creation zone de collisions des materiaux
for materiau in range(len(liste_materiaux_zone)):
    liste_materiaux_zone[materiau] = pygame.Rect(0.06 * SCREENWIDTH , 0.25 * SCREENHEIGHT + (0.055 * SCREENHEIGHT) * materiau, 0.17 * SCREENWIDTH, 0.06 * SCREENHEIGHT)

v_hauteur = variables(0.06455, str(liste_var_obj[0]),"hauteur",True)
v_masse = variables(0.135, str(liste_var_obj[1]),"masse",True)
v_viscosite = variables(0.597,str(liste_var_obj[2]),"viscosité",True)
v_g = variables(0.72,str(liste_var_obj[3]),"g",True)
v_rayon = variables(0.79,str(liste_var_obj[4]),"rayon",True)
v_v0 = variables(0.856,str(liste_var_obj[5]),"vit. init",True)


# MAIN
paused = True
x = 0


while running:
    try:
        
        hauteur = liste_var_obj[0]
        masse = liste_var_obj[1]
        viscosite = liste_var_obj[2]
        g = liste_var_obj[3]
        rayon = liste_var_obj[4]
        v0 = liste_var_obj[5]

        liste_var = [
                    v_hauteur,v_masse,v_viscosite,v_g,v_rayon,v_v0,variables(0.5,str(pdf_toggle),"PDF",False),
                    variables(0.065,str(round(boule.vitesse)),"m/s",False),variables(0.15,str(nb_rebond),"nb",False),variables(0.235,str(round(-boule.pos+hauteur_rebond)),"h",False),
                    variables(0.32,str(round(boule.force)),"N",False),variables(0.405,str(round(temps_global/60)),"t",False)
                   ]
        
        clock.tick(60)

        #affichage image de fond au coordonnees 0,0
        if viscosite != None:
            ciel.set_alpha(viscosite*255)
        else:
            ciel.set_alpha(255)
        screen.blit(main_background,(0,0))
        screen.blit(ciel,(0,0))

        #affichage objet qui tombe
        pygame.draw.circle(screen,[tableau_e['couleur_rouge'][new_index_materiaux],tableau_e['couleur_vert'][new_index_materiaux],tableau_e['couleur_bleu'][new_index_materiaux]], (SCREENWIDTH / 2, int(boule.pos-boule.rayon_maxetmin())), boule.rayon_maxetmin())

        #conditions de rebond
        if boule.pos > hauteur_rebond and t != 0:
            son.play()
            t = 0
            boule.pos = hauteur_rebond
            nb_rebond += 1
            boule.rebond()
    
        #on regarde tous les evenements de pygames
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE or event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                paused = not paused
            if event.type == pygame.KEYDOWN and event.key == pygame.K_DOWN:
                x += 1
                t += 1/60
                temps_global=temps_global+1
                boule.position_update(t)         
                print(boule.pos, boule.vitesse, x)
            if event.type == pygame.MOUSEBUTTONDOWN and pause_box.collidepoint(event.pos):
                 paused = not paused
        
            
            #regarde si on clique sur un des materiaux
            if event.type == pygame.MOUSEBUTTONDOWN:
                for zone_materiaux in range(len(liste_materiaux_zone)):
                    if liste_materiaux_zone[zone_materiaux].collidepoint(event.pos):
                        #si c'est le cas, nouveau materiau
                        new_index_materiaux = zone_materiaux

             # teste si on clique sur une variable, si oui recupere la potentielle nouvelle variable et l'affiche
            for variable in range(len(liste_var)):
                new_var = liste_var[variable].change_variable()
                if new_var != None:
                    if variable == 0:
                        new_var = -int(new_var) + hauteur_rebond
                    else:
                        new_var = float(new_var)
                    liste_var_obj[variable] = new_var
                    boule = objet(liste_var_obj[1],index_materiaux,None,liste_var_obj[0],liste_var_obj[4],liste_var_obj[5])
                    boule.vitesse = 0
                    boule.pos_init = liste_var_obj[0]
                    temps_global = 0
                    remise_a_zero_courbe()
                    nb_rebond = 0
                    paused = True
                new_var = None
        
            if event.type == pygame.MOUSEBUTTONDOWN and liste_var[6].box.collidepoint(event.pos):
                pdf_toggle = not pdf_toggle

        for variable in range(len(liste_var)):
            liste_var[variable].affiche_variable()

        #affiche les materiaux
        coord_background = (0.055 * SCREENWIDTH,0.205 * SCREENHEIGHT,0.19 * SCREENWIDTH,(0.065 * SCREENHEIGHT) * len(liste_materiaux))
        screen.fill((180,100,250),coord_background)
        pygame.draw.rect(screen,(0,0,0),coord_background,5)
        for affichage_materiaux in range(len(liste_materiaux)):
            if affichage_materiaux == new_index_materiaux:
                couleur_materiau = (80,0,230)           #le materiau actuel est d'une autre couleur
            else:
                couleur_materiau = (0,0,0)
            screen.blit(font.render(liste_materiaux[affichage_materiaux], True, (couleur_materiau)), (0.06 * SCREENWIDTH , 0.205 * SCREENHEIGHT + (0.065 * SCREENHEIGHT) * affichage_materiaux))

        if not paused:
            inchange=0                  #permet de définir qu'il y a de nouvelles info à rentrer dans la courbe
            temps_global=temps_global+1
            x += 1
            t += 1/60
            boule.position_update(t)         
            print(boule.pos, boule.vitesse, x)
            screen.fill((200,0,0),pause_box)
            pygame.draw.rect(screen,(0,0,0),pause_box,5)
            screen.blit(font_Large.render("PAUSE",True,(0,0,0)),(0.76 * SCREENWIDTH, 0.7 * SCREENHEIGHT))
            variable_dessin(boule.vitesse,temps_global,boule.pos,hauteur_rebond,boule.force)
        else:
            screen.fill((0,200,0),pause_box)
            pygame.draw.rect(screen,(0,0,0),pause_box,5)
            screen.blit(font_Large.render("START",True,(0,0,0)),(0.76 * SCREENWIDTH, 0.7 * SCREENHEIGHT))
            if pdf_toggle:
                if inchange ==  0:    
                    dessin()            #dessine la courbe
                    inchange=1            
                if x>=1:
                    son_pdf.play()
                    pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_WAIT)
                    dictionnaire_constante=v_lim()
                    dictionnaire_constante["Coefficient de restitution"]=str(boule.e)
                    pdf(dictionnaire_constante,boule.evolution)
                    son_pdf.play()
                    pygame.mouse.set_cursor(pygame.SYSTEM_CURSOR_ARROW)
                    

    except Exception as inst:
 
        print(inst)    # the exception type
 
        print(inst.args)     # arguments stored in .args
        messagebox.showinfo('Error','Nous avons rencontré un problème qui est:'+str(inst))         #envoi d'un message pour prévenir des changements de variables
        # boule = objet(masse,0,materiau,0,100,100)
        # t=0
        # paused = True

        running = False
    
    pygame.display.flip()
    
    
pygame.quit()