#Nous avons k différent suivant type de frottement: quadriatique (k′ = 1/2 η Cx S ) (rapide) ou frottement linéaires (k = 6 π η r) (lent)
#Nous définissons donc 2 variables k
import math
global exp
exp=math.e
#η=1.82*10**(-5)   #viscosité dynamique de l'air (en Pa.s) 



global pi
pi=math.pi
def k_vitesse_lent():
    k = 6 * pi * η * r
    return k

# Cx=0.45     #represente le coefficient de trainé ici d'une boule
# S = (4*m.pi*r)/2  #je n'en suis pas sure pour ce calcule mais est censé représenter la surface frontale de la boule voir perso.numericable.fr/gomars2/aero/cx_sphere.pdf page 14
def kprime_vitesse_rapide(η,Cx,S):
    k = (1/2)*η*Cx*S               #innutile
    return k



def valeur_de_base(constante_gravite,viscosite,rayon,masse):
    global r
    r=rayon
    global g
    g=constante_gravite
    global m
    m=masse
    global η
    if viscosite == None:
        η=0
    else:
        η=viscosite

#Soit T_bizzard une variable et T_bizzard=masse/k
def T_bizzard():
    T_bizzard=m/k_vitesse_lent()
    return T_bizzard
    
def vitesse_avec_frottement(t,h,v_init):
    if η != 0:
        T_biz = T_bizzard()
        v=-(g*T_biz*((exp**(-t/T_biz) -1))) + v_init      #exp et multiplication pas addition
        global vitessetropgrande
        vitessetropgrande=0
        print("limite non quadriatique:",g*T_biz)
        if v>(g*T_biz) or  v<(-g*T_biz):                                   #Si la vitesse est plus grande que la vitesse limite
            vitessetropgrande=1
            v=((v_init*exp**(-t/T_biz))) - g * T_biz * ((exp**(-t/T_biz))-1)    #test nouveau
    else:
        v=(g * (t)) + v_init
    return v

def position_avec_frottement(t,h,v_init):
    if η != 0:
        T_biz = T_bizzard()
        p=-(g*(T_biz**2)*(1-exp**(-(t/T_biz)))-g*T_biz*t)+h+(v_init*t)
        if vitessetropgrande==1:
            p=-(g*(T_biz**2)*(1-exp**(-(t/T_biz)))-g*T_biz*t)-((v_init*(exp**(-t/T_biz)))*T_biz)  +h +(v_init*T_biz)
    else:
        p=((g * (t**2))/2) + (v_init)*t + h
    return p

def v_lim():

    dictionnaire_constante={"masse (en kg)":m, "rayon (en m)":r,"constante de gravitation":g}
    print("aaaa")
    if η != 0 or η == None:
        dictionnaire_constante["n (la viscosité dynamique de l'air (en Pa.s))"]=η
        dictionnaire_constante["k (constante : 6* pi * n * r)"]=k_vitesse_lent()
        dictionnaire_constante["T (constante : m/k)"]=T_bizzard()
        dictionnaire_constante["vitesse limite"]=g*T_bizzard()
    return dictionnaire_constante