Voici un tutoriel pour expliquer comment utiliser ce programme :
Instructions d'installation et prérequis :

    Installation de Python et des bibliothèques nécessaires :
        Assurez-vous d'avoir Python installé sur votre système. Si ce n'est pas le cas, vous pouvez le télécharger et l'installer depuis python.org.
        Installez les bibliothèques requises en exécutant pip install pygame pandas.

    Organisation des fichiers :
        Placez tous les fichiers de votre projet dans un même répertoire. Assurez-vous que ce répertoire contient les fichiers image et son nécessaires (main_background.jpg, ciel.png, icone.png, boing.mp3, pdf.mp3).

Instructions d'utilisation :

    Exécution du programme :
        Exécutez le fichier main.py.

    Interface utilisateur :
        Lorsque vous exécutez le programme, une fenêtre de simulation apparaîtra en plein écran.
        Vous verrez une balle rebondissante dans cette fenêtre.

    Modification des variables :
        Sur le côté gauche de la fenêtre, vous trouverez plusieurs variables modifiables :
            Hauteur initiale (hauteur).
            Masse de la balle (masse).
            Viscosité (si applicable).
            Accélération due à la gravité (g).
            Rayon de la balle (rayon).
            Vitesse initiale de la balle (vit. init).
        Cliquez sur la variable que vous souhaitez modifier, entrez la nouvelle valeur à l'aide de votre clavier et appuyez sur "Entrée" pour valider.

    Interaction avec la simulation :
        Appuyez sur la touche ESPACE pour mettre en pause ou reprendre la simulation.
        Appuyez sur la touche FLECHE DU BAS pour avancer d'une étape dans la simulation lorsqu'elle est en pause.

    Sélection du matériau :
        Sur le côté gauche de la fenêtre, vous trouverez une liste de matériaux.
        Cliquez sur le nom d'un matériau pour le sélectionner. Cela affectera les propriétés de rebond de la balle.

    Observation des statistiques :
        Vous pouvez observer diverses statistiques en temps réel, telles que la vitesse de la balle (m/s), le nombre de rebonds (nb), la hauteur de la balle par rapport au sol (h), et la force exercée sur la balle (N).
        Ces statistiques sont affichées en haut de l'écran.

    Exportation de données :
        Vous pouvez exporter les données de simulation sous forme de fichier PDF en cliquant sur le bouton "PDF".
        Pour cela, assurez-vous que la simulation est en pause.
        Un fichier PDF sera généré, contenant les données de la simulation et une courbe représentant le mouvement de la balle.

    Fermeture du programme :
        Pour fermer la simulation, appuyez sur la touche ECHAP ou fermez la fenêtre.

C'est tout ce que vous devez savoir pour utiliser ce programme de simulation de rebondissement d'objet ! Amusez-vous bien !